#!/bin/bash
head -n 1000 2048000_names.txt > 1000_names.txt
head -n 2000 2048000_names.txt > 2000_names.txt
head -n 4000 2048000_names.txt > 4000_names.txt
head -n 8000 2048000_names.txt > 8000_names.txt
head -n 16000 2048000_names.txt > 16000_names.txt
head -n 32000 2048000_names.txt > 32000_names.txt
head -n 64000 2048000_names.txt > 64000_names.txt
head -n 128000 2048000_names.txt > 128000_names.txt
head -n 256000 2048000_names.txt > 256000_names.txt
head -n 512000 2048000_names.txt > 512000_names.txt
head -n 1024000 2048000_names.txt > 1024000_names.txt
