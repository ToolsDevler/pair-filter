#include <gtest/gtest.h>

#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "toolsdevler/pair_filter.hh"

TEST(BitarrayTest, Construction) {
  toolsdevler::detail::bitarray arr;
  EXPECT_TRUE(arr.empty());
}

TEST(BitarrayTest, Grow) {
  toolsdevler::detail::bitarray arr;
  toolsdevler::detail::set(arr, 0);
  EXPECT_FALSE(arr.empty());
  toolsdevler::detail::set(arr, 16);
  EXPECT_GT(arr.size(), 16);
  toolsdevler::detail::set(arr, 200);
  EXPECT_GT(arr.size(), 200);
}

TEST(BitarrayTest, Manipulation) {
  toolsdevler::detail::bitarray arr;
  toolsdevler::detail::set(arr, 0);
  EXPECT_TRUE(toolsdevler::detail::get(arr, 0));
  toolsdevler::detail::clear(arr, 0);
  EXPECT_FALSE(toolsdevler::detail::get(arr, 0));
}


TEST(PairFilterTest, SimpleExample) {
  // This test construct a simple scenario of a list with a subset of entries being marked as conflicting.
  std::vector<std::string> incredients{"salt", "sugar", "water", "oil", "flour"};
  std::map<int, int> conflicting{{0, 1}, {2, 3}};

  toolsdevler::pair_filter pf(incredients, conflicting);
  auto recept = pf(3);

  // Since we have 5 entries where 2 pairs are conflicting, we should get 3 entries out of it.
  EXPECT_EQ(recept.size(), 3);
  EXPECT_TRUE(std::ranges::find(recept, "salt") != std::end(recept) ^
              std::ranges::find(recept, "sugar") != std::end(recept));
  EXPECT_TRUE(std::ranges::find(recept, "water") != std::end(recept) ^
              std::ranges::find(recept, "oil") != std::end(recept));
  EXPECT_TRUE(std::ranges::find(recept, "flour") != std::end(recept));
}

TEST(PairFilterTest, CircularExclutionExample) {
  // This test constructs a non-trivial case where a chain of three entires can no
  std::vector<std::string> incredients{"salt", "sugar", "water", "oil", "flour"};
  std::map<int, int> conflicting{{0, 1}, {2, 3}};

  toolsdevler::pair_filter pf(incredients, conflicting);
  auto recept = pf(3);

  EXPECT_EQ(recept.size(), 3);
  EXPECT_TRUE(std::ranges::find(recept, "salt") != std::end(recept) ^
              std::ranges::find(recept, "sugar") != std::end(recept));
  EXPECT_TRUE(std::ranges::find(recept, "water") != std::end(recept) ^
              std::ranges::find(recept, "oil") != std::end(recept));
  EXPECT_TRUE(std::ranges::find(recept, "flour") != std::end(recept));
}

TEST(PairFilterTest, MultiExclusion) {
  // This test constructs a non-trivial case where a chain of three entires can no
  std::vector<std::string> incredients{"salt", "sugar", "water", "oil", "flour"};
  std::map<int, int> conflicting{{0, 3}, {2, 3}, {4, 3}};

  toolsdevler::pair_filter pf(incredients, conflicting);
  auto recept = pf(4);

  EXPECT_EQ(recept.size(), 4);
  EXPECT_TRUE(std::ranges::find(recept, "salt") != std::end(recept));
  EXPECT_TRUE(std::ranges::find(recept, "sugar") != std::end(recept));
  EXPECT_TRUE(std::ranges::find(recept, "water") != std::end(recept));
  EXPECT_TRUE(std::ranges::find(recept, "flour") != std::end(recept));
}

