#pragma once

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// I N C L U D E S
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <algorithm>
#include <bitset>
#include <cassert>
#include <chrono>
#include <filesystem>
#include <format>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <memory>
#include <ranges>
#include <set>
#include <unordered_map>
#include <utility>
#include <vector>

namespace toolsdevler {

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// H E L P E R S
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

namespace detail {

using bitarray = std::vector<bool>;

bool get(bitarray& arr, const int index) {
  if (index >= arr.size()) {
    arr.resize(index + 1);
  }
  return arr[index];
}

void set(bitarray& arr, const int index) {
  if (index >= arr.size()) {
    arr.resize(index + 1);
  }
  arr[index] = true;
}

void clear(bitarray& arr, const int index) {
  if (index >= arr.size()) {
    arr.resize(index + 1);
  }
  arr[index] = false;
}

}  // namespace detail

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// A P I
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <class container_t>
class pair_filter {
  using container_type = container_t;
  using value_type = container_type::value_type;

  /**
   * Holds every available entry.
   */
  container_t available_;

  /**
   * Defines pairs by hold indices of the entries that are not allowed to be
   * together in one list.
   */
  std::map<int, int> excluded_pairs_;

 public:
  pair_filter() = default;
  pair_filter(const container_t &available) : available_{available} {}
  pair_filter(const container_t &available, const std::map<int, int> &excluded_pairs)
      : available_{available}, excluded_pairs_{excluded_pairs} {}

  /**
   * Takes two entries into account that are not allowed to be in the filtered list.
   */
  void exclude(const value_type &lhs, const value_type &rhs) {
    int lhs_index = -1;
    int rhs_index = -1;
    if (auto it = std::ranges::find(available_, lhs); it != std::end(available_)) {
      lhs_index = std::distance(std::begin(available_), it);
    } else {
      throw std::runtime_error(std::format("'{}' is not part of the available entries!", lhs));
    }
    if (auto it = std::ranges::find(available_, rhs); it != std::end(available_)) {
      rhs_index = std::distance(std::begin(available_), it);
    } else {
      throw std::runtime_error(std::format("'{}' is not part of the available entries!", rhs));
    }

    exclude_by_index(lhs_index, rhs_index);
  }

  /**
   * Takes two entries into account that are not allowed to be in the filtered list.
   */
  void exclude_by_index(const int lhs_index, const int rhs_index) {
    if (lhs_index > rhs_index) {
      excluded_pairs_.emplace(lhs_index, rhs_index);
    } else if (lhs_index < rhs_index) {
      excluded_pairs_.emplace(rhs_index, lhs_index);
    } else {
      throw std::runtime_error(std::format("Attempted to exclude an entry '{}' with itself!", available_[lhs_index]));
    }
  }

  /**
   * Adds a new entry and returns its index.
   */
  auto add(const value_type &entry) -> int {
    available_.emplace_back(entry);
    return available_.size() - 1;
  }

  /**
   * Executes the filter operation and returns a filtered list without any of the pairs together in it.
   */
  auto operator()(const int amount) -> container_t {
    // Holds the state of the individual entries.
    detail::bitarray states(available_.size());
    // Remembers which bits have been updated in the set array.
    detail::bitarray touched(available_.size());

    /**
     * Updates a bit and makes sure, that the update is properly tracked.
     */
    auto update_bit = [&states, &touched](int index, bool one) {
      detail::set(touched, index);
      if (one) {
        detail::set(states, index);
      } else {
        detail::clear(states, index);
      }
    };

    container_t result;

    for (const auto &pair : excluded_pairs_) {
      if (detail::get(touched, pair.first)) {
        if (detail::get(states, pair.first)) {
          update_bit(pair.second, false);
        } else if (not detail::get(touched, pair.second)) {
          update_bit(pair.second, true);
        }
      } else if (detail::get(touched, pair.second)) {
        update_bit(pair.first, !detail::get(states, pair.second));
      } else {
        update_bit(pair.first, true);
        update_bit(pair.second, false);
      }
    }
    for (int i = 0; i < available_.size(); ++i) {
      if (detail::get(states, i) or not detail::get(touched, i)) {
        result.emplace_back(available_[i]);
        if (result.size() == amount) {
          return result;
        }
      }
    }
    return result;
  }
};

}  // namespace toolsdevler
