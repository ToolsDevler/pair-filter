# Pair Filter

A filter algorithm for generating lists without a specific set of pairs.

## Disclaimer

**This is a experimental and unmaintained project. Do not use anything here in production, or at least do not hold me accountable for any damage it might cause!**

And... There is countless problem instances that are linear solvable in an individual way. That does not mean that a solution to the whole NP problem situation has been found. The density of the exclusion pairs is so low which makes it an easy instance of an NP problem (if it is one at all). Most if not all of the test cases would be solvable by graph algorithms as well (see benchmark_graph). This is why pair-filter **is not** a proof that NP-Problems are solvable, nor do I expect to have a chance to get a million dollar price! **I just had a bit of fun!**

## Why would I use that?

When you have a large source of entries (This library is tested up to 2 million entries but it can handle more than that) and want to generate a list of entries that does not contain any pre-defined pairs.

## How does it work?

***Section in progress***

## Why did I waste my time on this?

I **love** to tinker with math and algorithmic problems, which is why I play around with different kind of theoretical and practical problems. This library was a result of thinking about the example problem described on the millenium problems page at the [Clay Mathematics Institute](https://www.claymath.org/) website.

The overview page of the P vs. NP millenium problem has an interesting example to chew on. You can read it here: [https://www.claymath.org/millennium/p-vs-np/](https://www.claymath.org/millennium/p-vs-np/).

So in my head I iterated through the information I have:

```
Students = [Student1, Student2, Student3, ..., StudentN];
Conflicts ={
    Student1: Student2,
    Student2: Student3,
    Student3: Student1,
    ...
};
Result = [ ??? ];
```



## Example

You have six incredients and want to exclude all that are conflicting with each other.

```cpp
#include "pair_filter.hh"

int main() {
    std::vector<std::string> incredients{
        "salt", "sugar", "water", "oil", "flour"
    };

    // Use the indices of the entries you want to add:
    std::map<int, int> conflicting{
        {0, 1}, // salt & sugar
        {2, 3} // water & oil
    };

    toolsdevler::pair_filter pf(conflicting, incredients);
    auto recept = pf(3);

    for (auto &e : recept) { 
        std::cout << recept << " ";
    }
    std::cout << std::endl;
    return 0;
}
```

Compile & Run:

```shell
$ g++ example.cc -o example && ./example
```

This will print:

```
flour salt water
```

