#include "toolsdevler/pair_filter.hh"

#include <benchmark/benchmark.h>

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

#include "benchmark.hh"

using namespace toolsdevler::benchmark;

std::vector<std::string> pair_filter(const std::vector<std::string>& ingredients, const std::map<int, int>& conflicts,
                                     int amount) {
  toolsdevler::pair_filter pf(ingredients, conflicts);
  return pf(amount);
}

int main(const int argc, const char* argv[]) {
  using namespace toolsdevler::benchmark;
  bool check_result = false;
  std::ofstream results{"pair_filter.results.csv", std::ios::trunc};
  auto fix = load_fixture("../test_data/400_names.txt", "../test_data/400_pairs.txt", pair_filter, 400 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "1000..." << std::endl;
  fix = load_fixture("../test_data/1000_names.txt", "../test_data/1000_pairs.txt", pair_filter, 1000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "2000..." << std::endl;
  fix = load_fixture("../test_data/2000_names.txt", "../test_data/2000_pairs.txt", pair_filter, 2000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "4000..." << std::endl;
  fix = load_fixture("../test_data/4000_names.txt", "../test_data/4000_pairs.txt", pair_filter, 4000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "8000..." << std::endl;
  fix = load_fixture("../test_data/8000_names.txt", "../test_data/8000_pairs.txt", pair_filter, 8000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "16000..." << std::endl;
  fix = load_fixture("../test_data/16000_names.txt", "../test_data/16000_pairs.txt", pair_filter, 16000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "32000..." << std::endl;
  fix = load_fixture("../test_data/32000_names.txt", "../test_data/32000_pairs.txt", pair_filter, 32000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "64000..." << std::endl;
  fix = load_fixture("../test_data/64000_names.txt", "../test_data/64000_pairs.txt", pair_filter, 64000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "128000..." << std::endl;
  fix = load_fixture("../test_data/128000_names.txt", "../test_data/128000_pairs.txt", pair_filter, 128000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "256000..." << std::endl;
  fix = load_fixture("../test_data/256000_names.txt", "../test_data/256000_pairs.txt", pair_filter, 256000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "512000..." << std::endl;
  fix = load_fixture("../test_data/512000_names.txt", "../test_data/512000_pairs.txt", pair_filter, 512000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "1024000..." << std::endl;
  fix = load_fixture("../test_data/1024000_names.txt", "../test_data/1024000_pairs.txt", pair_filter, 1024000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "2048000..." << std::endl;
  fix = load_fixture("../test_data/2048000_names.txt", "../test_data/2048000_pairs.txt", pair_filter, 2048000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);

  return 0;
}