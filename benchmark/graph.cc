#include <benchmark/benchmark.h>

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>

#include "benchmark.hh"

using namespace toolsdevler::benchmark;

std::vector<std::string> graphBasedSolution(const std::vector<std::string>& ingredients,
                                            const std::map<int, int>& conflicts, int amount) {
  std::vector<std::string> result;
  std::vector<bool> used(ingredients.size(), false);
  std::unordered_set<int> conflict_indices;

  // Build an adjacency list for conflicts
  std::vector<std::unordered_set<int>> conflict_graph(ingredients.size());
  for (const auto& [a, b] : conflicts) {
    if (a < ingredients.size() && b < ingredients.size()) {
      conflict_graph[a].insert(b);
      conflict_graph[b].insert(a);
    }
  }

  auto canUse = [&](int idx) {
    for (int neighbor : conflict_graph[idx]) {
      if (used[neighbor]) {
        return false;
      }
    }
    return true;
  };

  for (int i = 0; i < ingredients.size() && result.size() < amount; ++i) {
    if (canUse(i)) {
      result.push_back(ingredients[i]);
      used[i] = true;
    }
  }
  return result;
}

int main(const int argc, const char* argv[]) {
  using namespace toolsdevler::benchmark;
  bool check_result = false;
  std::ofstream results{"graph.results.csv", std::ios::trunc};
  auto fix = load_fixture("../test_data/400_names.txt", "../test_data/400_pairs.txt", graphBasedSolution, 400 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "1000..." << std::endl;
  fix = load_fixture("../test_data/1000_names.txt", "../test_data/1000_pairs.txt", graphBasedSolution, 1000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "2000..." << std::endl;
  fix = load_fixture("../test_data/2000_names.txt", "../test_data/2000_pairs.txt", graphBasedSolution, 2000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "4000..." << std::endl;
  fix = load_fixture("../test_data/4000_names.txt", "../test_data/4000_pairs.txt", graphBasedSolution, 4000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "8000..." << std::endl;
  fix = load_fixture("../test_data/8000_names.txt", "../test_data/8000_pairs.txt", graphBasedSolution, 8000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "16000..." << std::endl;
  fix = load_fixture("../test_data/16000_names.txt", "../test_data/16000_pairs.txt", graphBasedSolution, 16000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "32000..." << std::endl;
  fix = load_fixture("../test_data/32000_names.txt", "../test_data/32000_pairs.txt", graphBasedSolution, 32000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "64000..." << std::endl;
  fix = load_fixture("../test_data/64000_names.txt", "../test_data/64000_pairs.txt", graphBasedSolution, 64000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "128000..." << std::endl;
  fix = load_fixture("../test_data/128000_names.txt", "../test_data/128000_pairs.txt", graphBasedSolution, 128000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "256000..." << std::endl;
  fix = load_fixture("../test_data/256000_names.txt", "../test_data/256000_pairs.txt", graphBasedSolution, 256000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "512000..." << std::endl;
  fix = load_fixture("../test_data/512000_names.txt", "../test_data/512000_pairs.txt", graphBasedSolution, 512000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "1024000..." << std::endl;
  fix =
      load_fixture("../test_data/1024000_names.txt", "../test_data/1024000_pairs.txt", graphBasedSolution, 1024000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);
  std::cout << "2048000..." << std::endl;
  fix =
      load_fixture("../test_data/2048000_names.txt", "../test_data/2048000_pairs.txt", graphBasedSolution, 2048000 / 4);
  run(fix, check_result);
  dump_csv(fix, results);

  return 0;
}