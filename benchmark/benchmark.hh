#pragma once

#include <algorithm>
#include <chrono>
#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace toolsdevler::benchmark {

struct fixture {
  using algo_func =
      std::function<std::vector<std::string>(const std::vector<std::string>&, const std::map<int, int>&, int)>;
  int target = 0;
  std::vector<std::string> data;
  std::vector<std::string> result;
  algo_func func;
  std::map<int, int> pairs;
  double duration_ms = 0.0;
};

void run(fixture& fix, bool check_result = false) {
  const auto begin = std::chrono::system_clock::now();
  fix.result = fix.func(fix.data, fix.pairs, fix.target);
  const auto end = std::chrono::system_clock::now();
  fix.duration_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();

  if (check_result) {
    if (fix.result.size() < fix.target) {
      throw std::runtime_error(std::format("Algorithm failed to produce valid result! Expected {} results but got {}",
                                           fix.target, fix.result.size()));
    }
    for (const auto& pair : fix.pairs) {
      if (auto it = std::ranges::find(fix.result, fix.data[pair.first]); it != std::end(fix.result)) {
        if (auto jt = std::ranges::find(fix.result, fix.data[pair.second]); jt != std::end(fix.result)) {
          throw std::runtime_error(
              std::format("Algorithm failed to produce valid result! '{}' can not be together with '{}'",
                          fix.data[pair.first], fix.data[pair.second]));
        }
      }
    }
  }
}

void dump_csv(fixture& fix, std::ostream& out) {
  out << fix.result.size() << "," << fix.duration_ms << "\n";
  out.flush();
}

fixture load_fixture(const std::string& names, const std::string& pairs, fixture::algo_func func, int target) {
  fixture result;
  result.func = func;
  result.target = target;

  std::ifstream names_file{names};
  if (names_file.bad()) {
    perror(names.c_str());
    throw std::runtime_error(names + ":Unable to load file!");
  }

  std::string line;
  while (std::getline(names_file, line)) {
    result.data.emplace_back(line);
  }

  std::ifstream pairs_file{pairs};
  if (pairs_file.bad()) {
    perror(pairs.c_str());
    throw std::runtime_error(names + ":Unable to load file!");
  }

  while (std::getline(pairs_file, line)) {
    std::stringstream converter{line};
    int lhs, rhs;
    converter >> lhs;
    converter >> rhs;
    result.pairs.emplace(lhs, rhs);
  }

  return std::move(result);
}
}  // namespace toolsdevler::benchmark
